//Parameters for the coalescence simulation program : fastsimcoal.exe: H3: Recent geneflow
2 samples to simulate :
//Population effective sizes (number of genes)
south
north
//Samples sizes and samples age 
17
18
//Growth rates	: negative growth implies population expansion
0
0
//Number of migration matrices : 0 implies no migration between demes
2
//Migration matrix 0 
0 Z$
X$ 0
//Migration matrix 1
0 0
0 0
//historical event: time, source, sink, migrants, new deme size, new growth rate, migration matrix index
1 historical event
A$ 1 0 1 RESIZ 0 1
//Number of indeendent loci [chromosome]
1 0
//Per chromosome: Number of contiguous linkage Block: a block is a set of contiguous loci
1
//per Block:data type, number of loci, per generation recombination and muta
FREQ 1 0 1e-8 OUTEXP
