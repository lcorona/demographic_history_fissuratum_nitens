# Demographic History simulations for *Lophodermium nitens* and *L. fissuratum*

Salas-Lizana R., Figueroa-Corona L. & Oono R. 

#### Raw data are availble: 
[LINK]

## This repository reproduce the bioinformatic processing from  *"Genomic signatures of ancient vicariance in host-specific fungal endophytes across the Soltis Line of the Pacific Northwest"*
[LINK]

## Pre·Processing SFS construction 
for: *L. nitens* <br> 
<pre style="color: silver; background: black;">
./easySFS.py -i pop90red2.vcf -p nit_infopop\ \(1\).txt --unfolded --preview
./easySFS.py -i pop90red2.vcf -p nit_infopop\ \(1\).txt --unfolded --proj 17,32 </pre>

for: *L. fisuratum* <br> 
<pre style="color: silver; background: black;">
./easySFS.py -i fissuratum_pops9044.vcf -p fissu_ind_pop.txt --unfolded --preview
./easySFS.py -i fissuratum_pops9044.vcf -p fissu_ind_pop.txt --unfolded  --proj </pre>   

## Processing for Fastsimcoal v. 2.6 <br>

## *Lophodermium nitens* <br>
#### Simulations requeriments  <br>

<pre style="color: silver; background: black;">
PREFIX="nitt"
for i in {1..100}
 do
   mkdir run$i
   cp ${PREFIX}.tpl ${PREFIX}.est ${PREFIX}_MSFS.obs fsc26 run$i"/"
   cd run$i
   ./fsc26 -t ${PREFIX}.tpl -e ${PREFIX}.est -m -0 -C 10 -n 1000000 -L 20 -s0  -q -c 12 --multiSFS -M
   cd ..
 done
 </pre>

### Testing 4 scenarios
<br>

![4 scenarios]( https://gitlab.com/lcorona/demographic_history_fissuratum_nittens/-/raw/master/Figures/Fourscenarios.png)
<br>



## Isolation 
Assume any migration <br>

Simulation reconstruction <br>
<pre style="color: silver; background: black;">
//Parameters for the coalescence simulation program : fastsimcoal.exe: 
2 samples to simulate :
//Population effective sizes (number of genes)
north
south
//Samples sizes and samples age 
19
38
//Growth rates	: negative growth implies population expansion
0
0
//Number of migration matrices : 0 implies no migration between demes
0
//historical event: time, source, sink, migrants, new deme size, new growth rate, migration matrix index
1 historical event
TDIV 0 1 1 RESIZE 0 0
//Number of indeendent loci [chromosome]
1 0
//Per chromosome: Number of contiguous linkage Block: a block is a set of contiguous loci
1
//per Block:data type, number of loci, per generation recombination and muta
FREQ 1 0 1e-8 OUTEXP
</pre>

Parameters used
<pre style="color: silver; background: black;">
//  Search ranges and rules file
// *********************

[PARAMETERS]
//#isInt? #name   #dist.#min  #max 
//all Ns are in number of haploid individuals
1   ANCSIZE       logunif   100      1e5    output
1   north         logunif   100      1e5    output
1   south         logunif   100      1e5    output
1   TDIV          unif      1000     1e6    output

[RULES]

[COMPLEX PARAMETERS]
0  RESIZE = ANCSIZE/north  hide
</pre>

## Migration 

Asumming 4 different migration rates 

| Model  | Limits  | ---  |
| :---: | :---: | :---: |
| 1 | 1e-2 | 2 |
| 2 | 0.00001 |  0.1 |
| 3 | 1e-7  |   1e-5 |
| 4 | 1e-8 |  1e-7 |

Simulation reconstruction 

<pre style="color: silver; background: black;">
//Parameters for the coalescence simulation program : fastsimcoal.exe: nitt 75
2 samples to simulate :
//Population effective sizes (number of genes)
north
south
//Samples sizes and samples age 
19
38
//Growth rates	: negative growth implies population expansion
0
0
//Number of migration matrices : 0 implies no migration between demes
2
//Migration matrix 0
0 MIG21
MIG12 0
//Migration matrix 1
0 0
0 0
//historical event: time, source, sink, migrants, new deme size, new growth rate, migration matrix index
1 historical event
TDIV 0 1 1 RESIZE 0 1
//Number of indeendent loci [chromosome]
1 0
//Per chromosome: Number of contiguous linkage Block: a block is a set of contiguous loci
1
//per Block:data type, number of loci, per generation recombination and muta
FREQ 1 0 1e-8 OUTEXP
</pre>
<br>

Parameters used
<pre style="color: silver; background: black;">
//  Search ranges and rules file
// *********************

[PARAMETERS]
//#isInt? #name   #dist.#min  #max 
//all Ns are in number of haploid individuals
1   ANCSIZE       logunif   100   1e5    output
1   north         logunif   100   1e5  output
1   south         logunif   100   1e5  output
1   TDIV           unif      1000     1e6   output
0  MIG21          logunif  0.00001   0.1      output
0  MIG12          logunif  0.00001   0.1      output
[RULES]

[COMPLEX PARAMETERS]
0  RESIZE = ANCSIZE/north  hide
</pre>

## Secondary contact 
Asumming 4 different migration rates 
| Model  | Limits  | ---  |
| :---: | :---: | :---: |
| 1 | 1e-2 | 2 |
| 2 | 0.00001 |  0.1 |
| 3 | 1e-7  |   1e-5 |
| 4 | 1e-8 |  1e-7 |

Simulation reconstruction 
<pre style="color: silver; background: black;">
//Parameters for the coalescence simulation program : fastsimcoal.exe
2 samples to simulate :
//Population effective sizes (number of genes)
south
north
//Haploid samples sizes 
38
19
//Growth rates: negative growth implies population expansion
0
0
//Number of migration matrices : 0 implies no migration between demes
3
//Migration matrix 0
0	MIG1	  	
MIG2	0
//Migration matrix 1
0 	0
0	0
//Migration matrix 2
0	0   
0	0
//historical event: time, source, sink, migrants, new deme size, new growth rate, migration matrix index
2 historical event
TM 0 0 1 1 0 1
DI 0 1 1 RSANC 0 2
//Number of independent loci [chromosome] 
1 0
//Per chromosome: Number of contiguous linkage Block: a block is a set of contiguous loci
1
//per Block:data type, number of loci, per generation recombination and mutation rates and optional parameters
FREQ 1 0 1e-8 OUTEXP
</pre>
<br>

Parameters used
<pre style="color: silver; background: black;">
// Priors and rules file
// *********************

[PARAMETERS]
//#isInt? #name   #dist.#min  #max
//all Ns are in number of haploid individuals
1 	south	logunif	100	1e5	output
1	north	logunif	100	1e5	output
1	anc	logunif	100	1e5	output
1	TM	unif 1000     1e6	output
1	DI	unif 1000     1e6	output
0  MIG1      	logunif	0.00001 0.1   output
0  MIG2      	logunif	0.00001 0.1   output


[RULES]
TM < DI
[COMPLEX PARAMETERS]
1  RSANC = anc / north hide
</pre>
<br>

## Ancient migration  

Asumming 4 different migration rates 
| Model  | Limits  | ---  |
| :---: | :---: | :---: |
| 1 | 1e-2 | 2 |
| 2 | 0.00001 |  0.1 |
| 3 | 1e-7  |   1e-5 |
| 4 | 1e-8 |  1e-7 |

Simulation reconstruction 
<pre style="color: silver; background: black;"> 
//Parameters for the coalescence simulation program : fastsimcoal.exe
2 samples to simulate :
//Population effective sizes (number of genes)
north
south
//Haploid samples sizes 
19
38
//Growth rates: negative growth implies population expansion
0
0
//Number of migration matrices : 0 implies no migration between demes
3
//Migration matrix 0
0	0	  	
0  	0
//Migration matrix 1
0       MIG1
MIG2    0
//Migration matrix 2
0	0   
0       0
//historical event: time, source, sink, migrants, new deme size, new growth rate, migration matrix index
2 historical event
TM 0 0 0 1 0 1
DI 0 1 1 RSANC 0 2
//Number of independent loci [chromosome] 
1 0
//Per chromosome: Number of contiguous linkage Block: a block is a set of contiguous loci
1
//per Block:data type, number of loci, per generation recombination and mutation rates and optional parameters
FREQ 1 0 1e-8 OUTEXP
</pre>

Parameters used
<pre style="color: silver; background: black;">
// Priors and rules file
// *********************

[PARAMETERS]
//#isInt? #name   #dist.#min  #max
//all Ns are in number of haploid individuals
1  south		logunif	100	 1e5	 output
1  north		logunif	100	 1e5	 output
1	anc	logunif	 100	1e5	output
1	TM	unif  1000     1e6	output
1	DI      unif  1000     1e6	output
0  MIG1      	logunif	0.00001 0.1    output
0  MIG2      	logunif 0.00001 0.1    output

[RULES]
TM < DI
[COMPLEX PARAMETERS]
1 RSANC = anc / north hide
</pre>



## *Lophodermium fissuratum*

#### Simulations requeriments 
<br>
<pre style="color: silver; background: black;">
PREFIX="fiss"
for i in {1..100}
 do
   mkdir run$i
   cp ${PREFIX}.tpl ${PREFIX}.est ${PREFIX}_MSFS.obs fsc26 run$i"/"
   cd run$i
   ./fsc26 -t ${PREFIX}.tpl -e ${PREFIX}.est -m -0 -C 10 -n 1000000 -L 20 -s0  -q -c 12 --multiSFS -M
   cd ..
 done
 </pre>

 ### Testing 4 scenarios 

![4 scenarios]( https://gitlab.com/lcorona/demographic_history_fissuratum_nittens/-/raw/master/Figures/Fourscenarios.png)

 ## Isolation 
Assume any migration <br>
Simulation reconstruction 
<br>

<pre style="color: silver; background: black;">
//Parameters for the coalescence simulation program : fastsimcoal.exe:iso_fiss
2 samples to simulate :
//Population effective sizes (number of genes)
south
north
//Samples sizes and samples age 
17
18
//Growth rates	: negative growth implies population expansion
0
0
//Number of migration matrices : 0 implies no migration between demes
0
//historical event: time, source, sink, migrants, new deme size, new growth rate, migration matrix index
1 historical event
TDIV 0 1 1 RESIZ 0 0
//Number of indeendent loci [chromosome]
1 0
//Per chromosome: Number of contiguous linkage Block: a block is a set of contiguous loci
1
//per Block:data type, number of loci, per generation recombination and muta
FREQ 1 0 1e-8 OUTEXP
</pre>

Parameters used
<pre style="color: silver; background: black;">
//  Search ranges and rules file
// *********************

[PARAMETERS]
//  Search ranges and rules file
// *********************

[PARAMETERS]
//#isInt? #name   #dist.#min  #max 
//all Ns are in number of haploid individuals
1   south           logunif   100  1e5  output
1   north           logunif   100  1e5  output
1   NANC            logunif   100  1e5  output
1   TDIV              unif    1000 1e6  output
[RULES]
[COMPLEX PARAMETERS]
0 RESIZ = NANC/north      hide
</pre>

## Migration 

Asumming 4 different migration rates 
| Model  | Limits  | ---  |
| :---: | :---: | :---: |
| 1 | 1e-2 | 2 |
| 2 | 0.00001 |  0.1 |
| 3 | 1e-7  |   1e-5 |
| 4 | 1e-8 |  1e-7 |

Simulation reconstruction 
<br>

<pre style="color: silver; background: black;">
//Parameters for the coalescence simulation program : fastsimcoal.exe:iso_fiss
2 samples to simulate :
//Population effective sizes (number of genes)
south
north
//Samples sizes and samples age 
17
18
//Growth rates	: negative growth implies population expansion
0
0
//Number of migration matrices : 0 implies no migration between demes
2
//Migration matrix 0
0	MIG1	  	
MIG2	0
//Migration matrix 1
0	0   
0	0
//historical event: time, source, sink, migrants, new deme size, new growth rate, migration matrix index
1 historical event
TDIV 0 1 1 RESIZE 0 1
//Number of indeendent loci [chromosome]
1 0
//Per chromosome: Number of contiguous linkage Block: a block is a set of contiguous loci
1
//per Block:data type, number of loci, per generation recombination and muta
FREQ 1 0 1e-8 OUTEXP
</pre>

Parameters used
<pre style="color: silver; background: black;">
//  Search ranges and rules file
// *********************

[PARAMETERS]
//#isInt? #name   #dist.#min  #max 
//all Ns are in number of haploid individuals
1   south           logunif   100  1e5  output
1   north           logunif   100  1e5  output
1   anc	            logunif   100  1e5	output
1   TDIV              unif    1000 1e6  output
0  MIG1      	logunif	0.00001 0.1   output
0  MIG2      	logunif	0.00001 0.1   output
[RULES]
[COMPLEX PARAMETERS]
1  RESIZE = anc / north      hide
</pre>

## Secondary contact 
| Model  | Limits  | ---  |
| :---: | :---: | :---: |
| 1 | 1e-2 | 2 |
| 2 | 0.00001 |  0.1 |
| 3 | 1e-7  |   1e-5 |
| 4 | 1e-8 |  1e-7 |

<pre style="color: silver; background: black;">
//Parameters for the coalescence simulation program : fastsimcoal.exe
2 samples to simulate :
//Population effective sizes (number of genes)
south
north
//Haploid samples sizes 
17
18
//Growth rates: negative growth implies population expansion
0
0
//Number of migration matrices : 0 implies no migration between demes
3
//Migration matrix 0
0	MIG1	  	
MIG2	0
//Migration matrix 1
0 	0
0	0
//Migration matrix 2
0	0   
0	0
//historical event: time, source, sink, migrants, new deme size, new growth rate, migration matrix index
2 historical event
TM 0 0 1 1 0 1
DI 0 1 1 RSANC 0 2
//Number of independent loci [chromosome] 
1 0
//Per chromosome: Number of contiguous linkage Block: a block is a set of contiguous loci
1
//per Block:data type, number of loci, per generation recombination and mutation rates and optional parameters
FREQ 1 0 1e-8 OUTEXP
</pre>

Parameters used

<pre style="color: silver; background: black;">
// Priors and rules file
// *********************

[PARAMETERS]
//#isInt? #name   #dist.#min  #max
//all Ns are in number of haploid individuals
1 	south	logunif	100	1e5	output
1	north	logunif	100	1e5	output
1	anc	logunif	100	1e5	output
1	TM	unif 1000     1e6	output
1	DI	unif 1000     1e6	output
0  MIG1      	logunif	0.00001 0.1   output
0  MIG2      	logunif 0.00001 0.1   output

[RULES]
TM < DI
[COMPLEX PARAMETERS]
1  RSANC = anc / north  hide
</pre>
<br>

## Ancient migration 
Asumming 4 different migration rates 
| Model  | Limits  | ---  |
| :---: | :---: | :---: |
| 1 | 1e-2 | 2 |
| 2 | 0.00001 |  0.1 |
| 3 | 1e-7  |   1e-5 |
| 4 | 1e-8 |  1e-7 |

Simulation reconstruction 
<br>
<pre style="color: silver; background: black;">
//Parameters for the coalescence simulation program : fastsimcoal.exe
2 samples to simulate :
//Population effective sizes (number of genes)
north
south
//Haploid samples sizes 
18
17
//Growth rates: negative growth implies population expansion
0
0
//Number of migration matrices : 0 implies no migration between demes
3
//Migration matrix 0
0	0	  	
0  	0
//Migration matrix 1
0       MIG1
MIG2    0
//Migration matrix 2
0	0   
0       0
//historical event: time, source, sink, migrants, new deme size, new growth rate, migration matrix index
2 historical event
TM 0 0 0 1 0 1
DI 0 1 1 RSANC 0 2
//Number of independent loci [chromosome] 
1 0
//Per chromosome: Number of contiguous linkage Block: a block is a set of contiguous loci
1
//per Block:data type, number of loci, per generation recombination and mutation rates and optional parameters
FREQ 1 0 1e-8 OUTEXP
</pre>

Parameters used
<pre style="color: silver; background: black;">
// Priors and rules file
// *********************

[PARAMETERS]
//#isInt? #name   #dist.#min  #max
//all Ns are in number of haploid individuals
1  south		logunif	100	 1e5	 output
1  north		logunif	100	 1e5	 output
1	anc	logunif	 100	1e5	output
1	TM	unif  1000     1e6	output
1	DI	unif  1000     1e6	output
0  MIG1      	logunif	0.00001  0.1    output
0  MIG2      	logunif 0.00001  0.1    output
[RULES]
TM < DI
[COMPLEX PARAMETERS]
1 RSANC = anc / north hide
</pre>

### AIC calculation:
1. Select the best run from every parameter scenario (best.sh)

2. Calculate the AIC value to qualify the fit from each model (AIC.sh)

### Boostrap reconstruction: 


Results are avaible : https://docs.google.com/spreadsheets/d/16OCf0NGe0bdBWsYiJ4W6qJEwCMURMQIRxsGuqQmzmCE/edit#gid=0




